//
//  Location.swift
//  rainyshiny
//
//  Created by Marko Bizjak on 07/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import CoreLocation

class Location {
    
    static var sharedInstance = Location()
    private init() {}
    
    var latitude: Double!
    var longitude: Double!
    
}
