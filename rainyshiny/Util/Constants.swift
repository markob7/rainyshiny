//
//  Constants.swift
//  rainyshiny
//
//  Created by Marko Bizjak on 02/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import Foundation

let baseUrl = "http://api.openweathermap.org/data/2.5/weather?"
let latitude = "lat="
let longitude = "&lon="
let appID = "&appid="
let appKey = "60fd88ebe4fcab2a44c5df90fc9febe2"

typealias  DownloadComplete = () -> ()

let currentWeatherURL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&cnt=10&mode=json&appid=60fd88ebe4fcab2a44c5df90fc9febe2"
let forecast_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&cnt=10&mode=json&appid=60fd88ebe4fcab2a44c5df90fc9febe2"
