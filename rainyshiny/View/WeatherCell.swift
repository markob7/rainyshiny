//
//  WeatherCell.swift
//  rainyshiny
//
//  Created by Marko Bizjak on 07/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var highTemp: UILabel!
    @IBOutlet weak var lowTemp: UILabel!
    


    func configureCell(forecast: Forecast){
        
        lowTemp.text = "\(forecast.lowTemp)°"
        highTemp.text = "\(forecast.highTemp)°"
        weatherType.text = forecast._weatherType
        dayLabel.text = forecast._date
        weatherIcon.image = UIImage(named: forecast.weatherType)
    }

    

}
